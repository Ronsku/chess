import {Component} from 'angular2/core';
import {RouteConfig, Router, ROUTER_DIRECTIVES} from 'angular2/router';
import {FORM_PROVIDERS} from 'angular2/common';

import {RouterActive} from './directives/router-active';
import {Home} from './home/home';

@Component({
    selector: 'app',
    providers: [...FORM_PROVIDERS],
    directives: [...ROUTER_DIRECTIVES, RouterActive],
    pipes: [],
    styles: [`
    nav ul {
      display: inline;
      list-style-type: none;
      margin: 0;
      padding: 0;
      width: 60px;
    }
    nav li {
      display: inline;
    }
    nav li.active {
      background-color: lightgray;
    }
  `],
    template: `
    <header>
      <!-- Header content -->
    </header>

    <main>
      <router-outlet></router-outlet>
    </main>

    <footer>
        <!-- Footer content -->
    </footer>
  `
})
@RouteConfig([
    { path: '/', component: Home, name: 'Index' },
    { path: '/**', redirectTo: ['Index'] }
])
export class App {
    name = 'Chess demo';
    url = 'http://ronsku.com/';
    constructor() {

    }
}
