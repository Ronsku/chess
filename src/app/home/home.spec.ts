import {
it,
inject,
injectAsync,
describe,
beforeEachProviders,
TestComponentBuilder
} from 'angular2/testing';

import {Component, provide} from 'angular2/core';
import {BaseRequestOptions, Http} from 'angular2/http';
import {MockBackend} from 'angular2/http/testing';

import {Home} from './home';
import {Title} from './services/title';

describe('Home', () => {
    beforeEachProviders(() => [
        BaseRequestOptions,
        MockBackend,
        provide(Http, {
            useFactory: function(backend, defaultOptions) {
                return new Http(backend, defaultOptions);
            },
            deps: [MockBackend, BaseRequestOptions]
        }),

        Title,
        Home
    ]);

    it('should have a kingPos value', inject([Home], (home) => {
        expect(home.kingPos).toBeGreaterThan(0);
    }));

    it('should have a title', inject([Home], (home) => {
        expect(!!home.title).toEqual(true);
    }));
});
