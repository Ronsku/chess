describe('App', () => {

    beforeEach(() => {
        browser.get('/#/home');
    });

    it('should have a title', () => {
        let subject = browser.getTitle();
        let result = 'Chess demo';
        expect(subject).toEqual(result);
    });

    it('should have `Chess demo` x-large', () => {
        let subject = element(by.css('[x-large]')).getText();
        let result = 'Chess demo';
        expect(subject).toEqual(result);
    });


});
