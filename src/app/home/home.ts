import {Component} from 'angular2/core';
import {FORM_DIRECTIVES} from 'angular2/common';

import {Title} from './services/title';
import {XLarge} from './directives/x-large';
import {Cell} from './services/cell';

@Component({
    selector: 'home',
    providers: [
        Title
    ],
    directives: [
        ...FORM_DIRECTIVES,
        XLarge
    ],
    pipes: [],
    styles: [require('./home.css')],
    template: require('./home.html')
})

export class Home {
    cells: Object[] = new Array();
    // Generate a random number for kings position
    kingPos: number = Math.floor(Math.random() * 8 * 8);
    Ky: number;
    Kx: number;

    constructor(public title: Title) {
        this.Ky = Math.floor(this.kingPos / 8);
        this.Kx = (this.kingPos - (this.Ky * 8)) % 8;

        // Populate the cell array with Cell objects.
        for (var i = 0; i < (8 * 8); i++) {
            this.cells.push(new Cell(i, (i === this.kingPos ? true : false)));
        }
    }

    detectHit(id, event) {
        // Calculate the queen Row and Column for the queens position
        var Qy = Math.floor(id / 8),
            Qx = (id - (Qy * 8)) % 8;

        /*
        If queen is placed on the same row or column as the King.
        If queen is placed on the diagonal space from TOP LEFT to BOTTOM RIGHT.
        If queen is placed on the diagonal space from TOP RIGHT to BOTTOM LEFT.
        */
        if (this.Kx === Qx || this.Ky === Qy ||
           (this.Kx - this.Ky) === (Qx - Qy) ||
            Math.abs(this.Kx + this.Ky) === Math.abs(Qx + Qy)) {
            event.target.classList.add('flash');
        }
    }
}
