import {Injectable} from 'angular2/core';

@Injectable()
export class Cell {
    id: number;
    isKing: boolean;

    constructor(id, isKing) {
        this.id = id;
        this.isKing = isKing;
    }
}
